
$.fn.tabsBlock = function(){
	return this.each(function(){
		var $this = $(this),
			tabTitle = $this.find('.tabs-title li'),
			tabBlocks = $this.find('.tabs-block .tabs-b'),
			tabActive = 'active';

		tabTitle.on('click', onTabClick);

		function onTabClick(event){
			var target = $(event.target).closest('li'),
				index = target.index();

			tabTitle.removeClass(tabActive);
			target.addClass(tabActive);

			tabBlocks.removeClass(tabActive).eq(index).addClass(tabActive);
		}
	});
}



$(function(){
    // $('.grid').masonry({
    //   itemSelector: '.grid-item',
    //   columnWidth: 320
    // });

    $('.tab_block_wrap').tabsBlock();
    // const menuClone = $('.top_menu_header > ul').clone();

    // menuClone.appendTo('.menu_top');

    var $slider = $('.slider_main');

    if ($slider.find('.item').length > 1) {
        $slider.slick({
          dots: true,
          speed: 1000,
          arrows: true,
          fade: true,
          autoplay: true,
          autoplaySpeed: 3000,
          slidesToShow: 1,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 1300,
              settings: {
                  arrows: false
              }
            }
          ]
        });
    };

    let $sliderPopular = $('.slider_popular');
    let $sliderNew = $('.slider_new');
    let $sliderSale = $('.slider_sale');

    if ( window.matchMedia('(min-width : 768px)').matches) {

      if ($sliderPopular.find('.item_product').length > 1 && !$sliderPopular.hasClass('.slick-slider')) {
          $sliderPopular.slick({
            dots: false,
            speed: 500,
            arrows: true,
            autoplaySpeed: 3000,
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [
              {
                breakpoint: 1200,
                settings: {
                  arrows: false,
                  slidesToShow: 4
                }
              },
              {
                breakpoint: 1000,
                settings: {
                  arrows: false,
                    slidesToShow: 3
                }
              },
              {
                breakpoint: 767,
                settings: {
                  arrows: false,
                    slidesToShow: 2
                }
              }
            ]
          });
      };

      if ($sliderNew.find('.item_product').length > 1 && !$sliderNew.hasClass('.slick-slider')) {
          $sliderNew.slick({
            dots: false,
            speed: 500,
            arrows: true,
            autoplaySpeed: 3000,
            slidesToShow: 5,
            slidesToScroll: 1,
            responsive: [
              {
                breakpoint: 1200,
                settings: {
                  arrows: false,
                  slidesToShow: 4
                }
              },
              {
                breakpoint: 1000,
                settings: {
                  arrows: false,
                    slidesToShow: 3
                }
              },
              {
                breakpoint: 767,
                settings: {
                  arrows: false,
                    slidesToShow: 2
                }
              }
            ]
          });
      };

      if ($sliderSale.find('.item_product').length > 1 && !$sliderSale.hasClass('.slick-slider')) {
        $sliderSale.slick({
          dots: false,
          speed: 500,
          arrows: true,
          autoplaySpeed: 3000,
          slidesToShow: 5,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 1200,
              settings: {
                arrows: false,
                slidesToShow: 4
              }
            },
            {
              breakpoint: 1000,
              settings: {
                arrows: false,
                  slidesToShow: 3
              }
            },
            {
              breakpoint: 767,
              settings: {
                arrows: false,
                  slidesToShow: 2
              }
            }
          ]
        });
      };

    } else {
      if ($sliderPopular.hasClass('.slick-slider')) {
        $sliderPopular.slick('unslick');
      }

      if ($sliderNew.hasClass('.slick-slider')) {
        $sliderNew.slick('unslick');
      }

      if ($sliderSale.hasClass('.slick-slider')) {
        $sliderSale.slick('unslick');
      }        
    }

    var $sliderReview = $('.slider_review');

    if ($sliderReview.find('.item').length > 1) {
        $sliderReview.slick({
          dots: false,
          speed: 1000,
          arrows: true,
          autoplay: false,
          autoplaySpeed: 3000,
          slidesToShow: 1,
          slidesToScroll: 1
        });
    };

    var $sliderBrend = $('.brend_slider');

    if ($sliderBrend.find('.item').length > 1) {
        $sliderBrend.slick({
          dots: false,
          speed: 500,
          arrows: true,
          autoplaySpeed: 3000,
          slidesToShow: 5,
          slidesToScroll: 1,
          responsive: [
            {
              breakpoint: 1200,
              settings: {
                arrows: false,
                slidesToShow: 4
              }
            },
            {
              breakpoint: 767,
              settings: {
                arrows: false,
                  slidesToShow: 3
              }
            }
          ]
        });
    };

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        arrows: true,
        focusOnSelect: true
    });

	
	var $body = $(document.body),
      $html = $(document.documentElement);

  function formPopup($btn,$wrap){

    var closeForm = $('.formExtraWrapper .close-form'),
        formWrap = $($wrap),
        formBtn = $($btn),
        formOpened = 'opened',
        overflowHidden = 'oveflowHidden';

    closeForm.on('click', function() {
        formWrap.removeClass(formOpened);
        $html.removeClass(overflowHidden);
    });
    formBtn.on('click', function(event) {
        formWrap.addClass(formOpened);
        $html.toggleClass(overflowHidden);
        event.preventDefault();
    });

    $html.on('keyup', function(event) {
        if (formWrap.hasClass(formOpened) && event.keyCode == 27) {
            formWrap.removeClass(formOpened);
            $html.removeClass(overflowHidden);
        }
    });
    $body.on('click touchstart', function(a) {
        if ($(a.target).closest('.formExtraWrapper').length || $(a.target).closest(formBtn).length) return;
        if (formWrap.hasClass(formOpened)) {
            formWrap.removeClass(formOpened);
            $html.removeClass(overflowHidden);
        }
    });
  }

	formPopup('.call_btn','.code_form');
})

$(function(){
    var $html = $(document.documentElement),
        menuBtn = $('.burger'),
        menuWrapper = $('.menu_burger'),
        menuClose = $('.menuClose'),        
        openedMenu = 'opened',
        overflowHidden = 'oveflowHidden';

    menuBtn.on("click", function(event) {
        menuWrapper.toggleClass(openedMenu);
        menuBtn.toggleClass(openedMenu);
        $html.toggleClass(overflowHidden);
        $html.toggleClass('open_menu');
    });
    menuClose.on("click", function(event) {
        menuWrapper.removeClass(openedMenu);
        menuBtn.removeClass(openedMenu);
        $html.removeClass(overflowHidden);
        $html.removeClass('open_menu');
    });

    $(document).on('click touchstart', function(e){
        if( $(e.target).closest(menuBtn).length || $(e.target).closest(menuWrapper).length) 
          return;
        if (menuBtn.hasClass(openedMenu)){
            menuWrapper.removeClass(openedMenu);
            menuBtn.removeClass(openedMenu);
            $html.removeClass(overflowHidden);
            $html.removeClass('open_menu');
        }
    });

    $('.catalog .catalog_in').on('click', function(){
        $('.catalog').removeClass('active');
        $('.catalog').css('margin-bottom', 20);
        let $this = $(this).parent();
        let thisInnerBlockHeight = $this.find('.active_block').height(),
            thisPosition = $this.position(),
            topPosition = thisPosition.top,
            thisHeight = $this.height(),
            marginTop = thisInnerBlockHeight + 40,
            topActive = topPosition + thisHeight + 20;

        $this.addClass('active');
        $this.css('margin-bottom', marginTop);
        $this.find('.active_block').css('top', topActive);
    });

    $('.close_catalog').on('click', function(){
        $('.catalog').removeClass('active');
        $('.catalog').css('margin-bottom', 20);
    });

    $('.select_drop_name').on('click', function(){
        if ($(this).parent().hasClass('active')){
            $(this).parent().removeClass('active')
            $(this).parent().find('ul').slideUp();
        } else {
            $(this).parent().addClass('active');
            $(this).parent().find('ul').slideDown();
        }
    });

    $('.select_down li').on('click', function(){
        $('.select_down li').removeClass('active');
        $(this).addClass('active');
        let thisText = $(this).text();

        $(this).parents('.select_drop').find('.select_drop_name').text(thisText);
        $(this).parents('.select_drop').removeClass('active')
        $('.letters_box').removeClass('active');
        $(this).parent().slideUp();

        if ($(this).hasClass('select_latin')){
            $('.letters_box.latin').addClass('active');
        } else {
            $('.letters_box.cyrilic').addClass('active');
        }
    });

    $('.filter_field_name').on('click', function(){
        if ($(this).closest('.filter_field').hasClass('active')){
            $(this).closest('.filter_field').removeClass('active');
            $(this).closest('.filter_field').find('.filter_field_body').slideUp();
        } else {
            $(this).closest('.filter_field').addClass('active');
            $(this).closest('.filter_field').find('.filter_field_body').slideDown();
        }
    });

    if ($('#steps-slider').length) {
        var stepsSlider = document.getElementById('steps-slider');
        var input0 = document.getElementById('input-with-keypress-0');
        var input1 = document.getElementById('input-with-keypress-1');
        var inputs = [input0, input1];

        noUiSlider.create(stepsSlider, {
            start: [5000, 100000],
            connect: true,
            format: wNumb({
                decimals: 0
            }),
            range: {
                'min': [0],
                'max': 200000
            }
        });

        stepsSlider.noUiSlider.on('update', function (values, handle) {
            inputs[handle].value = values[handle];
        });
    }

    $('.filter_block .filter_checkbox label').on('click', function(){
        let thisPos = $(this).position(),
            topPos = thisPos.top;

        $('.result_filter').css('top', topPos);
                
        if (!$('.result_filter').hasClass('opened')) {
            $('.result_filter').addClass('opened');
        }
    });

    $(document).on('click touchstart', function(e){
        if( $(e.target).closest('.filter_block').length) 
          return;
        if ($('.result_filter').hasClass('opened')) {
            $('.result_filter').removeClass('opened');
        }
    });

    $('.filter_btn_mobile').on('click', function(){
        $('.filter_block').slideToggle();
    });

    $('.favorite_btn').on('click', function(){
        $(this).toggleClass('active');
    });

    $('.compare_btn').on('click', function(){
        $(this).toggleClass('active');
    });

    $('.add_address').on('click', function(){
        $(this).toggleClass('active');
        $('.add_address_form').toggleClass('opened');
    });

    $('.user_btn_main').on('click', function(){
        $(this).parent().addClass('opened');
    });

    $(document).on('click touchstart', function(e){
        if( $(e.target).closest('.user_btn').length) 
          return;
        if ($('.user_btn').hasClass('opened')) {
            $('.user_btn').removeClass('opened');
        }
    });
});


